//  A compiler from a very simple Pascal-like structured language LL(k)
//  to 64-bit 80x86 Assembly langage
//  Copyright (C) 2019 Pierre Jourlin
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//  
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//  
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.

// Build with "make compilateur"


#include <string>
#include <iostream>
#include <cstdlib>
#include <set>
#include <FlexLexer.h>
#include "tokeniser.h"
#include <cstring>

using namespace std;

enum OPREL {EQU, DIFF, INF, SUP, INFE, SUPE, WTFR};
enum OPADD {ADD, SUB, OR, WTFA};
enum OPMUL {MUL, DIV, MOD, AND ,WTFM};
enum WORDKEY {BEGIN, END, IF, THEN, ELSE,WHILE, FOR, DO, To, DISPLAY, WTFK};
enum ANALYSE{BOOLEAN,UNSIGNEDINT,DOUBLE,CHAR,WTFNULLOS};
TOKEN current;				// Current token
struct varr{
	char sg;
	ANALYSE pepe;
};
bool operator<(const varr& v1, const varr& v2) {
	return v1.sg < v2.sg;
}
FlexLexer* lexer = new yyFlexLexer; // This is the flex tokeniser
// tokens can be read using lexer->yylex()
// lexer->yylex() returns the type of the lexicon entry (see enum TOKEN in tokeniser.h)
// and lexer->YYText() returns the lexicon entry as a string

set<varr> levarss;
set<string> DeclaredVariables;
unsigned long TagNumber=0;

bool IsDeclared(const char *id){
	return DeclaredVariables.find(id)!=DeclaredVariables.end();
}


void Error(string s){
	cerr << "Ligne n°"<<lexer->lineno()<<", lu : '"<<lexer->YYText()<<"'("<<current<<"), mais ";
	cerr<< s << endl;
	exit(-1);
}

// Program := [DeclarationPart] StatementPart
// DeclarationPart := "[" Letter {"," Letter} "]"
// StatementPart := Statement {";" Statement} "."
// Statement := AssignementStatement
// AssignementStatement := Letter "=" Expression

// Expression := SimpleExpression [RelationalOperator SimpleExpression]
// SimpleExpression := Term {AdditiveOperator Term}
// Term := Factor {MultiplicativeOperator Factor}
// Factor := Number | Letter | "(" Expression ")"| "!" Factor
// Number := Digit{Digit}

// AdditiveOperator := "+" | "-" | "||"
// MultiplicativeOperator := "*" | "/" | "%" | "&&"
// RelationalOperator := "==" | "!=" | "<" | ">" | "<=" | ">="  
// Digit := "0"|"1"|"2"|"3"|"4"|"5"|"6"|"7"|"8"|"9"
// Letter := "a"|...|"z"
// Statement := AssignementStatement | IfStatement | WhileStatement | ForStatement | BlockStatement
// IfStatement := "IF" Expression "THEN" Statement [ "ELSE" Statement ]
// WhileStatement := "WHILE" Expression "DO" Statement
// ForStatement := "FOR" AssignementStatement "To" Expression "DO" Statement
// BlockStatement := "BEGIN" Statement { ";" Statement } "END"
	
		
ANALYSE Identifier(void){
	cout << "\tpush "<<lexer->YYText()<<endl;
	char s=lexer->YYText()[0];
	varr tmp={s};
	varr tmp2;
	tmp2=*levarss.find(tmp);
	current=(TOKEN) lexer->yylex();
	return tmp2.pepe;
}

ANALYSE Number(void){
	cout <<"\tpush $"<<atoi(lexer->YYText())<<endl;
	current=(TOKEN) lexer->yylex();
	return UNSIGNEDINT;
}

ANALYSE Expression(void);			// Called by Term() and calls Term()
void KeyWord(void);
WORDKEY KeyWordTest(void);
void IfStatement(void);
void WhileStatement(void);
void ForStatement(void);
void BeginStatement(void);
void Display(void);
ANALYSE Analyse(void);


ANALYSE Factor(void){
	if(current==RPARENT){
		current=(TOKEN) lexer->yylex();
		Expression();
		if(current!=LPARENT)
			Error("')' était attendu");		// ")" expected
		else
			current=(TOKEN) lexer->yylex();
		return BOOLEAN;
	}
	else{
		if (current==NUMBER){
			return Number();
		}
	    else{
				if(current==ID){
					return Identifier();
				}
				else{
					Error("'(' ou chiffre ou lettre attendue");
				}
		}
	}
	return WTFNULLOS;
}

// MultiplicativeOperator := "*" | "/" | "%" | "&&"
OPMUL MultiplicativeOperator(void){
	OPMUL opmul;
	if(strcmp(lexer->YYText(),"*")==0)
		opmul=MUL;
	else if(strcmp(lexer->YYText(),"/")==0)
		opmul=DIV;
	else if(strcmp(lexer->YYText(),"%")==0)
		opmul=MOD;
	else if(strcmp(lexer->YYText(),"&&")==0)
		opmul=AND;
	else opmul=WTFM;
	current=(TOKEN) lexer->yylex();
	return opmul;
}

// Term := Factor {MultiplicativeOperator Factor}
ANALYSE Term(void){
	OPMUL mulop;
	ANALYSE tmp;
	tmp=Factor();
	while(current==MULOP){
		mulop=MultiplicativeOperator();		// Save operator in local variable
		if(tmp!=Factor()&&current!=KEYWORD&&current!=DOT&&current!=SEMICOLON){
			Error("type d'analyse différent term");
		}
		cout << "\tpop %rbx"<<endl;	// get first operand
		cout << "\tpop %rax"<<endl;	// get second operand
		switch(mulop){
			case AND:
				cout << "\tmulq	%rbx"<<endl;	// a * b -> %rdx:%rax
				cout << "\tpush %rax\t# AND"<<endl;	// store result
				break;
			case MUL:
				cout << "\tmulq	%rbx"<<endl;	// a * b -> %rdx:%rax
				cout << "\tpush %rax\t# MUL"<<endl;	// store result
				break;
			case DIV:
				cout << "\tmovq $0, %rdx"<<endl; 	// Higher part of numerator  
				cout << "\tdiv %rbx"<<endl;			// quotient goes to %rax
				cout << "\tpush %rax\t# DIV"<<endl;		// store result
				break;
			case MOD:
				cout << "\tmovq $0, %rdx"<<endl; 	// Higher part of numerator  
				cout << "\tdiv %rbx"<<endl;			// remainder goes to %rdx
				cout << "\tpush %rdx\t# MOD"<<endl;		// store result
				break;
			default:
				Error("opérateur multiplicatif attendu");
		}
	}
	return tmp;
}

// AdditiveOperator := "+" | "-" | "||"
OPADD AdditiveOperator(void){
	OPADD opadd;
	if(strcmp(lexer->YYText(),"+")==0)
		opadd=ADD;
	else if(strcmp(lexer->YYText(),"-")==0)
		opadd=SUB;
	else if(strcmp(lexer->YYText(),"||")==0)
		opadd=OR;
	else opadd=WTFA;
	current=(TOKEN) lexer->yylex();
	return opadd;
}

// SimpleExpression := Term {AdditiveOperator Term}
ANALYSE SimpleExpression(void){
	OPADD adop;
	ANALYSE tmp;
	tmp=Term();
	while(current==ADDOP){
		adop=AdditiveOperator();		// Save operator in local variable
		if(tmp!=Term()&&current!=KEYWORD&&current!=DOT&&current!=SEMICOLON){
			Error("type d'analyse différent simple expr");
		}
		cout << "\tpop %rbx"<<endl;	// get first operand
		cout << "\tpop %rax"<<endl;	// get second operand
		switch(adop){
			case OR:
				cout << "\taddq	%rbx, %rax\t# OR"<<endl;// operand1 OR operand2
				break;			
			case ADD:
				cout << "\taddq	%rbx, %rax\t# ADD"<<endl;	// add both operands
				break;			
			case SUB:	
				cout << "\tsubq	%rbx, %rax\t# SUB"<<endl;	// substract both operands
				break;
			default:
				Error("opérateur additif inconnu");
		}
		cout << "\tpush %rax"<<endl;			// store result
	}
	return tmp;
}

// DeclarationPart := "[" Ident {"," Ident} "]"
void DeclarationPart(void){
	char tab[25];
	ANALYSE tab2[25];
	int o=0;
	if(current!=RBRACKET)
		Error("caractère '[' attendu");
	cout<<"FormatString1:    .string \"%llu\\n\"    # used by printf to display 64-bit unsigned integers"<<endl;
	cout << "FormatString2:\t.string \"%lf\\n\"\t# used by printf to display 64-bit floating point numbers"<<endl; 
	cout << "FormatString3:\t.string \"%c\\n\"\t# used by printf to display a 8-bit single character"<<endl; 
	cout << "\t.data"<<endl;
	cout << "\t.align 8"<<endl;
	
	current=(TOKEN) lexer->yylex();
	if(current!=ID)
		Error("Un identificater était attendu");
	//cout << lexer->YYText() << ":\t.quad 0"<<endl;
	DeclaredVariables.insert(lexer->YYText());
	tab[o++]=*lexer->YYText();
	current=(TOKEN) lexer->yylex();
	while(current==COMMA){
		current=(TOKEN) lexer->yylex();
		if(current!=ID)
			Error("Un identificateur était attendu");
		//cout << lexer->YYText() << ":\t.quad 0"<<endl;
		DeclaredVariables.insert(lexer->YYText());
		tab[o++]=*lexer->YYText();
		current=(TOKEN) lexer->yylex();
	}
	if(current!=LBRACKET)
		Error("caractère ']' attendu");
	current=(TOKEN) lexer->yylex();
	if(current==RBRACKET){
		int i=0;
		ANALYSE opp;
		while(current!=LBRACKET){
			current=(TOKEN) lexer->yylex();
			opp=Analyse();
			if(current!=COMMA)tab2[i++]=opp;

		}
	}
	while(o!=-1){
		varr tmp={tab[o],tab2[o]};
		levarss.insert(tmp);
		if(tab2[o]==BOOLEAN||tab2[o]==UNSIGNEDINT)
				cout<<tab[o]<<":\t.quad 0"<<endl;
		else if(tab2[o]==DOUBLE)
			cout<<tab[o]<<":\t.double 0.0"<<endl;
		else if(tab2[o]==CHAR)
			cout<<tab[o]<<":\t.byte 0"<<endl;

		o--;
	}
	current=(TOKEN) lexer->yylex();
}

// RelationalOperator := "==" | "!=" | "<" | ">" | "<=" | ">="  
OPREL RelationalOperator(void){
	OPREL oprel;
	if(strcmp(lexer->YYText(),"==")==0)
		oprel=EQU;
	else if(strcmp(lexer->YYText(),"!=")==0)
		oprel=DIFF;
	else if(strcmp(lexer->YYText(),"<")==0)
		oprel=INF;
	else if(strcmp(lexer->YYText(),">")==0)
		oprel=SUP;
	else if(strcmp(lexer->YYText(),"<=")==0)
		oprel=INFE;
	else if(strcmp(lexer->YYText(),">=")==0)
		oprel=SUPE;
	else oprel=WTFR;
	current=(TOKEN) lexer->yylex();
	return oprel;
}

// Expression := SimpleExpression [RelationalOperator SimpleExpression]
ANALYSE Expression(void){
	OPREL oprel;
	ANALYSE tmp;
	tmp=SimpleExpression();
	if(current==RELOP){
		oprel=RelationalOperator();
		if(tmp!=SimpleExpression()&&current!=KEYWORD&&current!=DOT&&current!=SEMICOLON){
			Error("type d'analyse différent expr");
		}
		cout << "\tpop %rax"<<endl;
		cout << "\tpop %rbx"<<endl;
		cout << "\tcmpq %rax, %rbx"<<endl;
		switch(oprel){
			case EQU:
				cout << "\tje Vrai"<<++TagNumber<<"\t# If equal"<<endl;
				break;
			case DIFF:
				cout << "\tjne Vrai"<<++TagNumber<<"\t# If different"<<endl;
				break;
			case SUPE:
				cout << "\tjae Vrai"<<++TagNumber<<"\t# If above or equal"<<endl;
				break;
			case INFE:
				cout << "\tjbe Vrai"<<++TagNumber<<"\t# If below or equal"<<endl;
				break;
			case INF:
				cout << "\tjb Vrai"<<++TagNumber<<"\t# If below"<<endl;
				break;
			case SUP:
				cout << "\tja Vrai"<<++TagNumber<<"\t# If above"<<endl;
				break;
			default:
				Error("Opérateur de comparaison inconnu");
		}
		cout << "\tpush $0\t\t# False"<<endl;
		cout << "\tjmp Suite"<<TagNumber<<endl;
		cout << "Vrai"<<TagNumber<<":\tpush $0xFFFFFFFFFFFFFFFF\t\t# True"<<endl;	
		cout << "Suite"<<TagNumber<<":"<<endl;
		return BOOLEAN;
	}
	return tmp;
}

// AssignementStatement := Identifier ":=" Expression
void AssignementStatement(void){
	ANALYSE tmp;
	char variable;
	if(current!=ID)
		Error("Identificateur attendu");
	if(!IsDeclared(lexer->YYText())){
		cerr << "Erreur : Variable '"<<lexer->YYText()<<"' non déclarée"<<endl;
		exit(-1);
	}
	variable=lexer->YYText()[0];
	current=(TOKEN) lexer->yylex();
	if(current!=ASSIGN)
		Error("caractères ':=' attendus");
	current=(TOKEN) lexer->yylex();
	tmp=Expression();
	if(tmp!=BOOLEAN&&current!=KEYWORD&&current!=DOT&&current!=SEMICOLON)
		Error("Assignement statement type différent");
	
	varr tmp3={variable};
	varr tmp2=*levarss.find(tmp3);
	switch(tmp2.pepe){
		case UNSIGNEDINT:
			cout << "\tpop "<<variable<<endl;
			break;
		case BOOLEAN:
			cout << "\tpop "<<variable<<endl;
			break;
		case DOUBLE:
			break;//a faire
		case CHAR:
			cout<<"\tpop %rdx"<<endl;
			cout<<"\tmovb %dl,"<<variable<<endl;
			break;
		default:
			Error("tkon");
	}
}

// Statement := AssignementStatement | Keyword
void Statement(void){
	if(current==KEYWORD)KeyWord();
	else AssignementStatement();
}

// StatementPart := Statement {";" Statement} "."
void StatementPart(void){
	cout << "\t.text\t\t# The following lines contain the program"<<endl;
	cout << "\t.globl main\t# The main function must be visible from outside"<<endl;
	cout << "main:\t\t\t# The main function body :"<<endl;
	cout << "\tmovq %rsp, %rbp\t# Save the position of the stack's top"<<endl;
	Statement();
	while(current==SEMICOLON){
		current=(TOKEN) lexer->yylex();
		Statement();
	}
	if(current!=DOT)
		Error("caractère '.' attendu");
	current=(TOKEN) lexer->yylex();
}

// Program := [DeclarationPart] StatementPart
void Program(void){
	if(current==RBRACKET)
		DeclarationPart();
	StatementPart();	
}

int main(void){	// First version : Source code on standard input and assembly code on standard output
	// Header for gcc assembler / linker
	cout << "\t\t\t# This code was produced by the CERI Compiler"<<endl;
	// Let's proceed to the analysis and code production
	current=(TOKEN) lexer->yylex();
	Program();
	// Trailer for the gcc assembler / linker
	cout << "\tmovq %rbp, %rsp\t\t# Restore the position of the stack's top"<<endl;
	cout << "\tret\t\t\t# Return from main function"<<endl;
	if(current!=FEOF){
		cerr <<"Caractères en trop à la fin du programme : ["<<current<<"]";
		Error("."); // unexpected characters at the end of program
	}

}
//Keyword := IfStatement | WhileStatement | ForStatement | BlockStatement	
void KeyWord(void){
	WORDKEY key;
	if(current==KEYWORD){
		key=KeyWordTest();
		switch(key){
			case IF:
				IfStatement();
				break;
			case WHILE:
				WhileStatement();
				break;
			case FOR:
				ForStatement();
				break;
			case BEGIN:
				BeginStatement();
				break;
			case DISPLAY:
				Display();
				break;
			default:
				Error("Keyword inconnu ");
		}
	}else {
		Error("acces keyword par erreur");
	}
}
//test keyword
WORDKEY KeyWordTest(void){
	WORDKEY key;
	if(strcmp(lexer->YYText(),"IF")==0)
		key=IF;
	else if(strcmp(lexer->YYText(),"THEN")==0)
		key=THEN;
	else if(strcmp(lexer->YYText(),"ELSE")==0)
		key=ELSE;
	else if(strcmp(lexer->YYText(),"WHILE")==0)
		key=WHILE;
	else if(strcmp(lexer->YYText(),"FOR")==0)
		key=FOR;
	else if(strcmp(lexer->YYText(),"DO")==0)
		key=DO;
	else if(strcmp(lexer->YYText(),"BEGIN")==0)
		key=BEGIN;
	else if(strcmp(lexer->YYText(),"END")==0)
		key=END;
	else if(strcmp(lexer->YYText(),"to")==0)
		key=To;
	else if(strcmp(lexer->YYText(),"DISPLAY")==0)
		key=DISPLAY;
	else key=WTFK;
	return key;
}
// IfStatement := "IF" Expression "THEN" Statement [ "ELSE" Statement ]
void IfStatement(void){
	cout<<"\t\t\t#IfStatement"<<endl;
	unsigned long truc = ++TagNumber;
	if(strcmp("IF",lexer->YYText())==0){
		current=(TOKEN) lexer->yylex();
		Expression();
	}else{
		Error("if attendu");
	}
	
	cout<<"\tpop %rcx\t\t#pop resultat"<<endl;
	cout<<"\tcmpq $0,%rcx\t\t#test si vrai ou faux"<<endl;
	cout<<"\tje faux"<<truc<<endl;
	if(strcmp("THEN",lexer->YYText())==0){
		current=(TOKEN) lexer->yylex();
		Statement();
	}else{
		Error("then attendu");
	}
	cout<<"\tjmp endif"<<truc<<endl;
	cout<<"faux"<<truc<<":"<<endl;
	if(strcmp("ELSE",lexer->YYText())==0){
		current=(TOKEN) lexer->yylex();
		Statement();
	}
	cout<<"endif"<<truc<<":"<<endl;
}
// WhileStatement := "WHILE" Expression "DO" Statement
void WhileStatement(void){
	cout<<"\t\t\t#WhileStatement"<<endl;
	unsigned long truc = ++TagNumber;
	if(strcmp("WHILE",lexer->YYText())==0){
		current=(TOKEN) lexer->yylex();
		Expression();
	}else{
		Error("While attendu");
	}
	cout<<"\tpop %rcx\t\t#pop resultat"<<endl;
	cout<<"boucle"<<truc<<":\tcmpq $0,%rcx\t\t#test si vrai ou faux"<<endl;
	cout<<"\tje faux"<<truc<<endl;
	if(strcmp("DO",lexer->YYText())==0){
		current=(TOKEN) lexer->yylex();
		Statement();
	}else{
		Error("do attendu");
	}
	cout<<"\tjmp boucle"<<truc<<endl;
	cout<<"faux"<<truc<<":"<<endl;
}

// ForStatement := "FOR" AssignementStatement "To" Expression "DO" Statement
void ForStatement(void){
	cout<<"\t\t\t#ForStatement"<<endl;
	string var;
	unsigned long truc =++TagNumber;
	if(strcmp("FOR",lexer->YYText())==0){
		current=(TOKEN) lexer->yylex();
		var=lexer->YYText();
		AssignementStatement();
	}else{
		Error("for attendu");
	}
	if(strcmp("To",lexer->YYText())==0){
		current=(TOKEN) lexer->yylex();
		Expression();
	}else{
		Error("to attendu ");
	}
	cout<<"\tpop %rcx"<<endl;
	cout<<"test"<<truc<<":\tcmpq %rcx,"<<var<<"\t\t#test for"<<endl;
	cout<<"\tje fin"<<truc<<endl;
	if(strcmp("DO",lexer->YYText())==0){
		current=(TOKEN) lexer->yylex();
		Statement();
	}else{
		Error("do attendu");
	}
	cout<<"\taddq $1,"<<var<<endl;
	cout<<"\tjmp test"<<truc<<endl;
	cout<<"fin"<<truc<<":"<<endl;
}
// BlockStatement := "BEGIN" Statement { ";" Statement } "END"
void BeginStatement(void){
	unsigned long truc = ++TagNumber;
	cout<<"\t\t\t#begin"<<truc<<endl;
	if(strcmp("BEGIN",lexer->YYText())==0){
		current=(TOKEN) lexer->yylex();
		Statement();
	}else{
		Error("begin attendu");
	}
	if(strcmp(";",lexer->YYText())==0){
		current=(TOKEN) lexer->yylex();
		Statement();
	}
	if(strcmp("END",lexer->YYText())==0){
		cout<<"\t\t\t#end"<<truc<<endl;
	}else{
		Error("end attendu");
	}
}
//display 
void Display(void){
	current=(TOKEN) lexer->yylex();
	ANALYSE tmp;
	tmp=Expression();
	switch(tmp){
		case UNSIGNEDINT:
			cout<<"\tpop %rdx                     # The value to be displayed"<<endl;
			cout<<"\tmovq $FormatString1, %rsi    # \"%llu\\n\""<<endl;
			cout<<"\tmovl    $1, %edi"<<endl;
			cout<<"\tmovl    $0, %eax"<<endl;
			cout<<"\tcall    __printf_chk@PLT"<<endl;
			break;
		case BOOLEAN:
			cout<<"\tpop %rdx                     # The value to be displayed"<<endl;
			cout<<"\tmovq $FormatString1, %rsi    # \"%llu\\n\""<<endl;
			cout<<"\tmovl    $1, %edi"<<endl;
			cout<<"\tmovl    $0, %eax"<<endl;
			cout<<"\tcall    __printf_chk@PLT"<<endl;
			break;
		case CHAR:
			cout<<"\tpop %rsi\t\t\t# get character in the 8 lowest bits of %si"<<endl;
			cout << "\tmovq $FormatString3, %rdi\t# \"%c\\n\""<<endl;
			cout << "\tmovl	$0, %eax"<<endl;
			cout << "\tcall	printf@PLT"<<endl;
			break;
		default:
			break;
	}
}
//enum ANALYSE{EXPRESSION,MUNBER,IDENTIFIER,BOOLEAN,UNSIGNED_INT,WTFNULLOS};
ANALYSE Analyse(void){
	ANALYSE key;
	if(strcmp(lexer->YYText(),"BOOLEAN")==0)
		key=BOOLEAN;
	else if(strcmp(lexer->YYText(),"UNSIGNEDINT")==0)
		key=UNSIGNEDINT;
	else if(strcmp(lexer->YYText(),"DOUBLE")==0)
		key=DOUBLE;
	else if(strcmp(lexer->YYText(),"CHAR")==0)
		key=CHAR;
	else key=WTFNULLOS;
	return key;
}
